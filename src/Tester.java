import alimentari.Alimento;
import alimentari.Carne;
import alimentari.Vino;
import ristorazioni.Enoteca;
import ristorazioni.EnotecaBio;
import ristorazioni.Ristorante;
import ristorazioni.Ristorazione;

import java.io.IOException;

public class Tester {
    public static void main(String[] args) {

        Alimento filetto = new Carne("maiale","coscia","ehi");
        Alimento garzellino = new Vino("garzellino","tipo","bianco",13.5);

        System.out.println(filetto);
        System.out.println(garzellino);

        Ristorazione mrHop = new Ristorante("mrHop");
        Ristorazione laBottega = new Enoteca("La Bottega");

        laBottega.addAlimento(filetto,30);
        laBottega.addAlimento(garzellino,2);
        mrHop.addAlimento(filetto,13);

        System.out.println(laBottega);
        System.out.println(mrHop);
        try {
            Ristorazione test = new Ristorante("prova", "./risorse/menu1.txt");
            System.out.println(test);
        }catch(IOException e){
            System.err.println(e.getMessage());
        }

    }
}
