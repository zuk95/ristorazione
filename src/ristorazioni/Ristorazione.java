package ristorazioni;

import alimentari.Alimento;

public interface Ristorazione {
    boolean addAlimento(Alimento a,double prezzo);
    boolean rmvAlimento(Alimento a);
    String toString();
    String getNomeRistorazione();
}
