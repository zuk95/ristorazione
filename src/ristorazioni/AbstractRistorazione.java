package ristorazioni;

import alimentari.Alimento;
import alimentari.Carne;
import alimentari.Vino;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public abstract class AbstractRistorazione implements Ristorazione {
    private String nomeRistorazione;
    private Menu m;
    public AbstractRistorazione(String nomeRistorazione) {
        this.nomeRistorazione = nomeRistorazione;
        this.m = new Menu();
    }

    public AbstractRistorazione(String nomeRistorazione,String nomeFile) throws IOException {
        this.nomeRistorazione = nomeRistorazione;
        this.m = new Menu();
        BufferedReader in = null;
            in = new BufferedReader(new FileReader(nomeFile));
            String line;
            while ((line = in.readLine()) != null) {
                creaAlimento(line);
            }
            in.close();
    }


    public String getNomeRistorazione() {
        return nomeRistorazione;
    }

    @Override
    public boolean addAlimento(Alimento a,double prezzo) {
        return m.addAlimento(a,prezzo);
    }

    @Override
    public boolean rmvAlimento(Alimento a) {
        return  m.rmvAlimento(a);
    }

    @Override
    public String toString() {
        return "Ristorazione "+nomeRistorazione+"\n"+
                m.toString();
    }

    private void creaAlimento(String line){
        StringTokenizer st = new StringTokenizer(line,"\t");
        while(st.hasMoreTokens()) {
            st.nextToken();
            String nome = st.nextToken();
            if (line.startsWith("0")) {
                String provenienza = st.nextToken();
                String pezzo = st.nextToken();
                double prezzo = Double.valueOf(st.nextToken());
                this.addAlimento(new Carne(nome,provenienza, pezzo), prezzo);
            }
            if(line.startsWith("1")){
                double gradazioneAlcolica = Double.valueOf(st.nextToken());
                String tipo = st.nextToken();
                String colore = st.nextToken();
                double prezzo = Double.valueOf(st.nextToken());
                this.addAlimento(new Vino(nome,tipo,colore,gradazioneAlcolica),prezzo);
            }
        }
    }


}
