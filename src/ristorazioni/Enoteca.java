package ristorazioni;

import alimentari.Alimento;
import alimentari.Vino;

import java.io.IOException;

public class Enoteca extends AbstractRistorazione {
    public Enoteca(String nomeRistorazione) {
        super(nomeRistorazione);
    }

    public Enoteca(String nomeRistorazione, String nomeFile) throws IOException {
        super(nomeRistorazione, nomeFile);
    }

    @Override
    public boolean addAlimento(Alimento a, double prezzo) {
        if(a instanceof Vino){
            return super.addAlimento(a,prezzo);
        }else{
            return false;
        }
    }
}
