package ristorazioni;

import alimentari.Alimento;

import java.util.ArrayList;

public class Menu {
    private ArrayList<Alimento> lista;

    public Menu() {
        this.lista = new ArrayList<>();
    }

    public boolean addAlimento(Alimento a,double prezzo){
        a.setPrezzo(prezzo);
        return lista.add(a);
    }


    public boolean rmvAlimento(Alimento a){
        return lista.remove(a);
    }

    @Override
    public String toString() {
        return "MENU\n"
                +"---------------\n"
                +lista+
                "\n-------------\n";
    }
}
