package ristorazioni;

import alimentari.Alimento;
import alimentari.AlimentoBio;

import java.io.IOException;

public class EnotecaBio extends Enoteca {
    public EnotecaBio(String nomeRistorazione) {
        super(nomeRistorazione);
    }

    public EnotecaBio(String nomeRistorazione, String nomeFile) throws IOException {
        super(nomeRistorazione, nomeFile);

    }

    @Override
    public boolean addAlimento(Alimento a, double prezzo) {
        if(a instanceof AlimentoBio) {
            return super.addAlimento(a, prezzo);
        }else{
            return false;
        }
    }
}
