package ristorazioni;

import java.io.IOException;

public class Ristorante extends AbstractRistorazione{
    public Ristorante(String nomeRistorazione) {
        super(nomeRistorazione);
    }

    public Ristorante(String nomeRistorazione, String nomeFile) throws IOException {
        super(nomeRistorazione, nomeFile);
    }
}
