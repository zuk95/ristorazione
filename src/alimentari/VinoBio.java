package alimentari;

public class VinoBio extends Vino implements AlimentoBio {
    private String codTack;

    public VinoBio(String nome, String tipo, String colore, double gradazioneAlcolica, String codTack) {
        super(nome, tipo, colore, gradazioneAlcolica);
        this.codTack = codTack;
    }

    @Override
    public String getCodTack() {
        return codTack;
    }

    @Override
    public String toString() {
        return super.toString()+"|"+codTack+super.toString()+"\n";
    }

}
