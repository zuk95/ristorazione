package alimentari;

public class Vino extends AbstractAlimento {
    private String tipo;
    private String colore;
    private double gradazioneAlcolica;

    public Vino(String nome, String tipo, String colore, double gradazioneAlcolica) {
        super(nome);
        this.tipo = tipo;
        this.colore = colore;
        this.gradazioneAlcolica = gradazioneAlcolica;
    }

    @Override
    public String toString() {
        return getNome()+ "|"+tipo+"|"+colore+"|"+gradazioneAlcolica+super.toString()+"\n";
    }
}
