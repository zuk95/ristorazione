package alimentari;

public class AbstractAlimento implements Alimento{
    private double prezzo;
    private String nome;

    public AbstractAlimento(String nome) {
        this.nome=nome;
    }

    @Override
    public String getNome() {
        return nome;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    @Override
    public String toString() {
        return "    PREZZO:" + prezzo;
    }
}
