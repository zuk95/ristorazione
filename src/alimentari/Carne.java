package alimentari;

public class Carne extends AbstractAlimento {
    private String provenienza;
    private String pezzo;

    public Carne(String nome, String provenienza, String pezzo) {
        super(nome);
        this.provenienza = provenienza;
        this.pezzo = pezzo;
    }

    @Override
    public String toString() {
        return getNome() + "|" + provenienza + "|"+pezzo+super.toString()+"\n";
    }
}
